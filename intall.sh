#!/bin/sh

ROOT_NAME="root"
SLEEP_TIME=5
MENSAGE_ROOT="[ ALERT ] - Favor execute como root (sudo su)"

verify_user(){
    if [ "$USER" != "$ROOT_NAME" ]; then
        echo $MENSAGE_ROOT
        echo " -> Usuário atual: $USER"
        echo "Digite em seu terminal -> sudo sh install.sh"
        read ""PRESS
        exit
    fi
}

verify_user

cd ..
mv hoppscotch /snap/hoppscotch
mv /snap/hoppscotch/hoppscotch.desktop /usr/share/applications

